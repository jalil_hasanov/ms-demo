FROM arm64v8/openjdk:11.0.9-jdk
RUN mkdir /app
COPY ./build/libs/ms-demo-0.0.1-SNAPSHOT.jar /app/ms-demo-0.0.1-SNAPSHOT.jar
WORKDIR /app
EXPOSE 8080
CMD ["java", "-jar", "ms-demo-0.0.1-SNAPSHOT.jar"]

