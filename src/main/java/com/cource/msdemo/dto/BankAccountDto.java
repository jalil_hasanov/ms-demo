package com.cource.msdemo.dto;

import com.cource.msdemo.model.enums.Currency;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BankAccountDto {
    String accountNumber;
    @Enumerated(EnumType.ORDINAL)
    Currency currency;
    BigDecimal balance;
    LocalDate creationDate;
}
