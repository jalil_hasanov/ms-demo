package com.cource.msdemo.mapstruct;

import com.cource.msdemo.dto.BankAccountDto;
import com.cource.msdemo.model.BankAccount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


import java.util.List;

@Mapper(componentModel = "spring")
public abstract class BankAccountMapstruct {

    public abstract List<BankAccountDto> mapAllEntityToDtos(List<BankAccount> all);

    @Mapping(target = "id", ignore = true)
    public abstract BankAccount mapDtoToEntity(BankAccountDto request);

    public abstract BankAccountDto mapEntityToDto(BankAccount bankAccount);

}
