package com.cource.msdemo.repo;

import com.cource.msdemo.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount,Long> {

    @Override
    BankAccount save(BankAccount entity);
}
