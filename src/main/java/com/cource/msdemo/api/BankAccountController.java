package com.cource.msdemo.api;


import com.cource.msdemo.dto.BankAccountDto;
import com.cource.msdemo.model.BankAccount;
import com.cource.msdemo.model.BankAccountListResponse;
import com.cource.msdemo.service.BankAccountService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bank/account")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class BankAccountController {
    BankAccountService service;

    @PostMapping("/create")
    public ResponseEntity<BankAccountDto> create(@RequestBody BankAccountDto request){
        return ResponseEntity.ok(service.create(request));
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<BankAccountDto> get(@PathVariable("id")Long id){
        return ResponseEntity.ok(service.get(id));
    }

    @GetMapping("/findAll")
    public ResponseEntity<BankAccountListResponse>findAll(){
        return ResponseEntity.ok(service.findAll());
    }


}
