package com.cource.msdemo.service;

import com.cource.msdemo.dto.BankAccountDto;
import com.cource.msdemo.model.BankAccount;
import com.cource.msdemo.model.BankAccountListResponse;

public interface BankAccountService {
    BankAccountDto create(BankAccountDto request);

    BankAccountDto get(Long id);

    BankAccountListResponse findAll();
}
