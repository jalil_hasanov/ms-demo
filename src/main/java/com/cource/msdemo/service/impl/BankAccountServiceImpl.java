package com.cource.msdemo.service.impl;

import com.cource.msdemo.dto.BankAccountDto;
import com.cource.msdemo.exceptions.MissingBankAccountException;
import com.cource.msdemo.mapstruct.BankAccountMapstruct;
import com.cource.msdemo.model.BankAccount;
import com.cource.msdemo.model.BankAccountListResponse;
import com.cource.msdemo.repo.BankAccountRepository;
import com.cource.msdemo.service.BankAccountService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository repository;
    private final BankAccountMapstruct mapstruct;

    @Override
    public BankAccountDto create(BankAccountDto request) {
        return mapstruct.mapEntityToDto(repository.save(mapstruct.mapDtoToEntity(request)));
    }

    @Override
    public BankAccountDto get(Long id) {
        return mapstruct
                .mapEntityToDto(Optional.of(repository.findById(id)).orElseThrow(MissingBankAccountException::new).get());
    }

    @Override
    public BankAccountListResponse findAll() {
        List<BankAccountDto>bankAccountDtoList = mapstruct.mapAllEntityToDtos(repository.findAll());
        return BankAccountListResponse.builder()
                .bankAccounts(bankAccountDtoList)
                .build();
    }
}
