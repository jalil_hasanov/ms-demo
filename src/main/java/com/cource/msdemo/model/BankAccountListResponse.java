package com.cource.msdemo.model;

import com.cource.msdemo.dto.BankAccountDto;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BankAccountListResponse {
    List<BankAccountDto>bankAccounts;
}
