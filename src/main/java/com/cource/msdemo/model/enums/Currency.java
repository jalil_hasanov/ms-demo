package com.cource.msdemo.model.enums;

public enum Currency {
    AZN("azn"),USD("usd");

    String val;


    Currency(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
