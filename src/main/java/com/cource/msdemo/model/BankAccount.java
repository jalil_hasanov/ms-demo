package com.cource.msdemo.model;

import com.cource.msdemo.model.enums.Currency;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    Long id;
    String accountNumber;
    @Enumerated(EnumType.STRING)
    Currency currency;
    BigDecimal balance;
    LocalDate creationDate;
}
