package com.cource.msdemo.exceptions;

import lombok.experimental.StandardException;

@StandardException
public class MissingBankAccountException extends RuntimeException {
    public MissingBankAccountException() {
        super("no such bank account", new NullPointerException());
    }
}
