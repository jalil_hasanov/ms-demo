package com.cource.msdemo.service.impl;

import com.cource.msdemo.dto.BankAccountDto;
import com.cource.msdemo.mapstruct.BankAccountMapstruct;
import com.cource.msdemo.model.BankAccount;
import com.cource.msdemo.model.enums.Currency;
import com.cource.msdemo.repo.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BankAccountServiceImplTest {

    @Mock
    BankAccountRepository repository;

    @Spy
    BankAccountMapstruct mapstruct;

    @InjectMocks
    BankAccountServiceImpl service;

    BankAccount bankAccount;

    BankAccountDto dto;

    @BeforeEach
    void setUp() {
        bankAccount = BankAccount.builder()
                .accountNumber("1234567")
                .balance(BigDecimal.valueOf(300.3))
                .creationDate(LocalDate.of(2022,10,12))
                .currency(Currency.AZN)
                .build();
        dto = BankAccountDto.builder()
                .accountNumber("1234567")
                .balance(BigDecimal.valueOf(300.3))
                .creationDate(LocalDate.of(2022,10,12))
                .currency(Currency.AZN)
                .build();
    }

    @Test
    void create() {
        when(mapstruct.mapDtoToEntity(dto)).thenReturn(bankAccount);
        when(repository.save(bankAccount)).thenReturn(bankAccount);
        when(mapstruct.mapEntityToDto(bankAccount)).thenReturn(dto);

        BankAccountDto bankAccountTest = service.create(dto);

        assertThat(bankAccountTest.getCurrency()).isEqualTo(Currency.AZN);

        verify(mapstruct,times(1)).mapDtoToEntity(dto);
        verify(repository, times(1)).save(bankAccount);
        verify(mapstruct, times(1)).mapEntityToDto(bankAccount);
    }

    @Test
    void get() {

    }

    @Test
    void findAll() {
    }
}